package com.example.mirsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MirsaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MirsaApplication.class, args);
	}

}
